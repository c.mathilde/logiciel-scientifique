def pgcd(a, b):
    if a < b:
        return(pgcd(b, a))
    if b == 0:
        return(a)
    if a > b:
        n = a%b
        return(pgcd(b, n))

print("q1.", pgcd(123456, 234567))

def phi(n):
    res = 0
    for i in range(n):
        if pgcd(i, n) == 1:
            res += 1
    return(res)

print("q2.", phi(10))
print(phi(30))





def farey(n):
    if (n in (0,1)):
        return(2)
    res = 1
    for i in range(n+1):
        res += phi(i)
    return(res)

print("q3.", farey(1))

for i in range (10, 31):
    print ("q4. n="+str(i), farey(i))