def fibo_liste(n):
    if n == 0:
        return([0])
    if n == 1:
        return([0,1])
    L = [0,1]
    for i in range (2, n+1):
        m = L[i-1]+L[i-2]
        L.append(m)
    return(L)

print("q1.", fibo_liste(5))

u0 = fibo_liste(0)
verifU0 = [0]

if (u0!=verifU0) :
    print("FAUX")

def fibo(n):
    if n == 0:
        return(0)
    if n == 1:
        return(1)
    un2 = 0
    un1 = 1
    res = un2 + un1
    i = 2
    while (i < n):
        un2 = un1
        un1 = res
        res = un2 + un1
        i += 1
    return(res)

print("q2.", fibo(5))
for i in range (21):
    print("pour n="+str(i), fibo(i))

def fibo_pair(n):
    if n == 0:
        return(0)
    if n == 1:
        return(0)
    un2 = 0
    un1 = 1
    un = un2 + un1
    i = 2
    res = 0
    while (i < n):
        un2 = un1
        un1 = un
        un = un2 + un1
        i += 1
        if (un%2 == 0):
            res += un
    return(res)

print("q3.", fibo_pair(100000)%10000007)