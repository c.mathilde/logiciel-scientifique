def algo_euclide(a, b):
    if a < b:
        return(algo_euclide(b, a))
    if b == 0:
        return(a)
    if a > b:
        n = a%b
        return(algo_euclide(b, n))

def pgcd(a, b):
    if a < b:
        return(pgcd(b, a))
    u = a
    v = b
    while v != 0:
        r = u%v
        u = v
        v = r
    return(u)
    
print(algo_euclide(495, 275))
print(pgcd(275, 495))
print(pgcd(10, 5))