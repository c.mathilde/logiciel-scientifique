from math import factorial

def inverse(L) :
    T = L[::-1]
    return(T)

def dec(B) :
    c= 0
    I = inverse(B)
    for i in range (0, len(B)) :
        c = c + ((2 ** i) * I[i])
    return c

def binaire(n) :
    p= 0
    L = []
    while (2**p) < n :
        p = p + 1
    for i in range (p-1, -1, -1) :
        if (2**i) <= n :
            L.append(1)
            n = n - (2**i)
        else :
            L.append(0)
    return L

L = [1,1,0,1]
A = [1, 1, 0, 1, 1, 0, 0]
B = [1, 0, 1, 0, 1, 0, 1, 0]
n = 13
print(inverse(L))
print(dec(L))
print(dec(A) + dec(B))
print(binaire(n))

print("q5.",binaire(dec(A) + dec(B)))

print("q6.", binaire(factorial(50)))

print("q6.", len(binaire(factorial(50))))