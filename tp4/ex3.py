import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, 1, 400)
i = 0
for t in np.arange(0.4, 3, 0.01):
    plt.plot(x, x**t, color=plt.cm.jet(i))
    i += 1
#○plt.show()

#x = np.linspace(0, 1, 200)
#plt.plot(x, x**2, label="carre")
#plt.plot(x, x**3, label="cube")
#♥plt.legend(loc=0)
plt.title("...")
plt.show()