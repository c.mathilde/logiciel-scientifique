import matplotlib.pyplot as plt
import numpy as np


def fibo(debut, fin):
    u0 = 0
    u1 = 1
    L = []
    for i in range(2,fin+1):
        u = u0 + u1
        u0 = u1
        u1 = u
        if i >= debut:
            L.append(u)
    return L

plt.yscale('log')
plt.plot(fibo(5, 25))
plt.show()
