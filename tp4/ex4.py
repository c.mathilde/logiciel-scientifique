import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-10, 10, 400)
y = (-1 * (1 / 4)) * (x - 2) + (1 / 2)
plt.plot(x, 1/x)
plt.plot(x, y)
plt.axis("equal")
plt.show()