#!/bin/python
n = 3
while n < 10:
    print(n, n**2)
    n += 1


def plusPetitEntier(n):
    m = n
    while (m*(m+1))/2 < 1000000:
        m +=1
    return m

print(plusPetitEntier(1))