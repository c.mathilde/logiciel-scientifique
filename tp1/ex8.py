#!/bin/python
def div35(n):
    m = 0
    for i in range(0, n):
        if i % 3 == 0 or i % 5 == 0:
            m += i
    return(m)

print(div35(10))
print(div35(1000))
print(div35(2000) + div35(3000))