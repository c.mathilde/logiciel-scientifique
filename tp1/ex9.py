#!/bin/python
def syracuse(n, o=1):
    if n == 1:
        return(o)
    if n % 2 == 0:
        n /= 2
    else:
        n = ((n*3)+1) / 2
    o += 1
    return(syracuse(n, o))

print(syracuse(10))
print(syracuse(100))

def suiteSyracuse(n, m):
    L = []
    for i in range (n, m+1):
        L.append(syracuse(i))
    return(L)

res = suiteSyracuse(10, 10000)

print(suiteSyracuse(10, 25))
print("le max est : ")
print(max(res))

def leMax(liste):
    lmax = 0
    imax = 0
    for i in range(0, len(liste)):
        if liste[i] > lmax:
            lmax = liste[i]
            imax = i
    return(lmax, imax)

print(leMax(res))